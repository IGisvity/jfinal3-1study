package com.demo.index;

import com.jfinal.core.Controller;
import com.jfinal.i18n.I18n;
import com.jfinal.i18n.Res;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * 本 demo 仅表达最为粗浅的 jfinal 用法，更为有价值的实用的企业级用法
 * 详见 JFinal 俱乐部: http://jfinal.com/club
 * 
 * IndexController
 */
public class IndexController extends Controller {
	public void index() {
//		Res resEn = I18n.use("en_US");
//		String msgEn = resEn.get("msg");
		render("index.html");
	}

	public void changeLanguage(){
		String local = getCookie("_locale");
		if(local.equals("zh_CN")){
			setCookie("_locale","en_US",1000);
		}else {
			setCookie("_locale","zh_CN",1000);
		}

		redirect("/");
	}
}





