package com.demo.user;

import com.demo.common.model.Sex;

import java.util.List;

public class SexService {
    private final static Sex dao = new Sex().dao();

    public List<Sex> getSexList(){
        return dao.find("select * from sex");
    }
}
