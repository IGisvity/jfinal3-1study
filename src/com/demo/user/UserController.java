package com.demo.user;

import com.demo.common.model.Sex;
import com.demo.common.model.User;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

public class UserController extends Controller {
    static UserService service= new UserService();
    static SexService sexService = new SexService();
    public void index(){
        setAttr("userPage",service.paginate(getParaToInt(0,1),10));
        render("user.html");
    }

    public void delete(){
        service.deleteById(getParaToInt());
        redirect("/user");
    }

    public void edit(){
        setAttr("sexlist", sexService.getSexList());
        setAttr("user", service.findById(getParaToInt()));
    }
    @Before(UserValidator.class)
    public void updata()
    {
        getModel(User.class).update();
        redirect("/user");
    }

    public void add(){
        setAttr("sexlist", sexService.getSexList());
    }

    @Before(UserValidator.class)
    public void save(){
        getModel(User.class).save();
        redirect("/user");
    }
}
