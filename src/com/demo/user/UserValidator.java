package com.demo.user;

import com.demo.common.model.User;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class UserValidator extends Validator {
    private SexService sexService =new SexService();
    @Override
    protected void validate(Controller c) {
        validateRequiredString("user.username","namemsg","请输入用户名");
        validateRequiredString("user.password","passwordmsg","请输入密码");
    }

    @Override
    protected void handleError(Controller c) {
        c.keepModel(User.class);
        String actionKey = getActionKey();
        c.setAttr("sexlist", sexService.getSexList());
        switch (actionKey){
            case "/user/save":
                controller.render("add.html");
                break;

            case "/user/update":
                controller.render("add.html");
                break;
        }
    }
}
