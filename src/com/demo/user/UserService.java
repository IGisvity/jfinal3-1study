package com.demo.user;

import com.demo.common.model.User;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

public class UserService {

    private final static User dao = new User().dao();

    public Page<User> paginate(int pageNumber,int pageSize){
        return dao.paginate(pageNumber,pageSize,"select u.*,s.sexname "," from User u LEFT JOIN sex s on u.sexid = s.id order by u.id desc");
    }


    public void deleteById(int id){
        dao.deleteById(id);
    }

    public User findById(int paraToInt) {
        String sql ="SELECT u.*,s.sexname from user u LEFT JOIN sex s on u.sexid = s.id where u.id = ?";
        return dao.findFirst(sql,paraToInt);
    }
}
